#! /usr/bin/env python3

# Replace the below URL with the URL of the RNG to be used.
url = "http://127.0.0.1:8090/ws"

# Python Library for fetching random numbers from RNG Peerplays
# Last modified on 29th of May 2020, by Jemshid j.kuzhiyil@pbsa.info
import sys
import time
import requests
import ast
import numpy as np
import yaml
import matplotlib.pyplot as plt
# plt.ion()


with open("config.yaml") as configFile:
    config = yaml.safe_load(configFile)
    url = config["url"]


helpText = \
    """\
    def Rng(min=0, max=2, count=1, repeat=True, filenameToStore=None):
    def Hist(r):
    def PlotHist(): This method plots the histogram of Peerplays RNG and Numpy RNG for comparison,
        The RNG fetch is made similar to a Keno call.
    def Billion(batchCount, fileNameToStore): #batches of size 100K
    """

data = dict()
data["method"] = "call"
params = ["database", "get_random_number_ex", [1, 48, 7, False]]
data["params"] = params
data["jsonrpc"] = "2.0"
data["id"] = 1


def Rng(min=0, max=2, count=1, repeat=True, filenameToStore=None):
    """
    The basic RNG puller
    """
    if (int(min) < 0) or (int(max) < 0):
        print("The RNG works for positive range only")
        return None
    argParams = [min, max, count, repeat]
    params[2] = argParams
    data["params"] = params
    # print(params)
    r = requests.post(url, json=data)
    r = r.text
    r = ast.literal_eval(r)
    if "result" in r:
        r = r["result"]
        if not isinstance(filenameToStore, type(None)):
            fileNameToStore = str(filenameToStore)
            rString = str(r)
            rString = rString[1:-1]
            rString = rString.replace(',', '')
            with open(fileNameToStore, "a") as f:
                f.write(rString)
                f.write(" ")
                # f.write("\n")
        return r
    else:
        print("Error, check arguments")
        return None


def RngDie(min=0, max=2, count=1, repeat=True, filenameToStore=None):
    """
    This method is to save random numbers in a file in such a way that is suitable for Diehards testing
    """
    argParams = [min, max, count, repeat]
    params[2] = argParams
    data["params"] = params
    r = requests.post(url, json=data)
    r = r.text
    r = ast.literal_eval(r)
    if "result" in r:
        r = r["result"]
        if not isinstance(filenameToStore, type(None)):
            fileNameToStore = str(filenameToStore)
            rString = str(r)
            rString = rString[1:-1]
            rString = rString.replace(',', '')
            rString = rString.replace(' ', '\n')
            rString = rString + "\n"
            with open(fileNameToStore, "a") as f:
                f.write(rString)
                # f.write(" ")
                # f.write("\n")
        return r
    else:
        print("Error, check arguments")
        return None


def Hist(r, maxR=80):
    """
    Generates histogram of the random numbers in r
    """
    if type(maxR) == type(None):
        maxR = np.max(r) + 1
    hist = np.zeros(maxR)
    for i in r:
        hist[i] = hist[i] + 1
    return hist


def PlotHist():
    """
    Plots Histogram of random numbers from the peerplays RNG and Numpy RNG for comparison.
    The RNG fetches at batch size of 20 with max = 80.
    """
    maxR = 80
    # rnFromPeerplays = KenoSample(batchCount=1000)
    rnFromPeerplays = Rng(0,80, 20000)
    rnFromPeerplays = np.array(rnFromPeerplays)
    rnFromPeerplays = rnFromPeerplays.flatten()
    histRnFromPeerplays = Hist(rnFromPeerplays, maxR=80)
    histRnFromPeerplays = 1.0 * maxR * histRnFromPeerplays / len(rnFromPeerplays)

    rnFromNp = np.random.randint(0, 80, len(rnFromPeerplays))
    histRnFromNp = Hist(rnFromNp, maxR=80)
    histRnFromNp = 1.0 * maxR * histRnFromNp / len(rnFromNp)

    # Random score
    deviationFromRandomForPeerplays = histRnFromPeerplays.max() - histRnFromPeerplays.min()
    deviationFromRandomForNp = histRnFromNp.max() - histRnFromNp.min()
    randomScore = deviationFromRandomForNp / deviationFromRandomForPeerplays
    print("RandomScore=", randomScore)

    plt.plot(histRnFromPeerplays, 'r')
    plt.plot(histRnFromNp, 'g')
    plt.plot(histRnFromPeerplays, 'r*')
    plt.plot(histRnFromNp, 'g*')
    plt.legend(["RNGPeerPlays", "RNGNumpy"])
    plt.title("Histograms of RNG")
    plt.xlabel("Random Number")
    plt.ylabel("Normalized Probability of Occurrence")
    plt.show()
    return randomScore



def Billion(batchCount, fileNameToStore):  # batches of size 100K
    """
    A method dedicated to generate a billion random numbers pretty fast with 100K batch size
    """
    for k in range(batchCount):
        print(k)
        tic = time.time()
        rng100k = Rng(0, 100, 10 ** 5, True, fileNameToStore)
        print(k, "/", batchCount, '   TimeTaken:', time.time() - tic)


def KenoSample(batchCount=1, filenameToStore=None):  # batches of size 100K
    """
    A sample Keno Call
    """
    rngAll = []
    tic0 = time.time()
    for k in range(batchCount):
        tic = time.time()
        rngTrash = Rng(min=0, max=80, count=20, repeat=False, filenameToStore=filenameToStore)
        rngTrash = rngTrash[0:20]
        print(k, "/", batchCount, "  ", time.time() - tic)
        toc0 = time.time() - tic0
        print(toc0, toc0 / (k + 1))
        rngAll.append(rngTrash)
    return rngAll


if __name__ == "__main__":
    if url == "http://127.0.0.1:8090/ws":
        print("Replace URL in config.yanml with the URL of the witness")

    # print("len sys.arv:", len(sys.argv))
    # for k in range(len(sys.argv)):
    #    print(k, sys.argv[k])
    # print("len sys.arv:", len(sys.argv))
    # for k in range(len(sys.argv)):
    #    print(k, sys.argv[k])
    if len(sys.argv) == 1:
        print(helpText)
    elif sys.argv[1] == "Billion":
        if len(sys.argv) >= 4:
            batchCount = int(sys.argv[2])
            fileNameToStore = sys.argv[3]
            Billion(batchCount, fileNameToStore)
    elif sys.argv[1] == "PlotHist":
        PlotHist()
    elif sys.argv[1] == "Rng":
        min = 0
        max = 2
        count = 1
        repeat = True
        fileNameToStore = None
        if len(sys.argv) >= 3:
            min = sys.argv[2]
        if len(sys.argv) >= 4:
            max = sys.argv[3]
        if len(sys.argv) >= 5:
            count = sys.argv[4]
        if len(sys.argv) >= 6:
            repeat = sys.argv[5] == "True"
        if len(sys.argv) >= 7:
            fileNameToStore = sys.argv[6]
        r = Rng(min, max, count, repeat, fileNameToStore)
        print(r)
    else:
        print(helpText)
